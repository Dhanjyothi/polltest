package com.hcl.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.hcl.dao.EmployeeDao;
import com.hcl.model.Employee;
import com.hcl.util.ConnectionData;

public class Main {
    static ConnectionData connection = new ConnectionData();
    static Connection con;
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
      create(1,"revathi",30);  
      fetch();
    }
    
    public static  int create(Integer id, String name, Integer age) throws SQLException, ClassNotFoundException {
      // System.out.println("Insertion");
        con = connection.getConnection();
        PreparedStatement stmt=con.prepareStatement("insert into emp values(?,?,?)");
        stmt.setInt(1,id);
        stmt.setString(2,name);
        stmt.setInt(3,age);
        int result=stmt.executeUpdate();
        return result;
        
     
    }

    public static List fetch() throws ClassNotFoundException, SQLException {
        List list=new ArrayList();
        Connection con = connection.getConnection();
       PreparedStatement stmt=con.prepareStatement("select * from emp");
      ResultSet rs=stmt.executeQuery();
      while(rs.next())
      {
         list.add(rs.getInt(1));
         list.add(rs.getString(2));
         list.add(rs.getInt(3));
         
      }
      return list;
    }
    public int update(String name,Integer age,Integer id) throws ClassNotFoundException, SQLException {
       /* Connection con = connection.getConnection();
        PreparedStatement stmt=con.prepareStatement("update emp set name=? ,age=? where id=?");
        stmt.setString(1,"sindhu");
        stmt.setInt(2,38);
        stmt.setInt(3,1);
        int result=stmt.executeUpdate();*/
        return 1;
      
    }

    public int delete(Integer Id) throws ClassNotFoundException, SQLException {
        return 1;
      
    }

}
